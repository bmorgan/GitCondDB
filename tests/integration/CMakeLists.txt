###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the Apache version 2        #
# licence, copied verbatim in the file "COPYING".                             #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
cmake_minimum_required(VERSION 3.10)
project(integration VERSION 0.0.0)

include(GNUInstallDirs)

option(USE_BUILTIN_GITCONDDB "Choose between builtin or external build of GitCondDB")
if(USE_BUILTIN_GITCONDDB)
  add_subdirectory(GitCondDB)
else()
  find_package(GitCondDB REQUIRED)
endif()

add_executable(integration_test src/main.cpp)
target_link_libraries(integration_test GitCondDB::GitCondDB)

install(TARGETS integration_test
        RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})
