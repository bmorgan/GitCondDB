/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the Apache version 2        *
* licence, copied verbatim in the file "COPYING".                             *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <GitCondDB.h>
#include <iostream>

int main() {
  auto db = GitCondDB::connect( R"(json:
    {"TheDir": {"TheFile.txt": "some JSON (memory) data\n"}}
  )" );

  if ( !db.connected() ) {
    std::cerr << "failed to connect to Git CondDB (in memory JSON)\n";
    return EXIT_FAILURE;
  }

  if ( std::get<0>( db.get( {"HEAD", "TheDir/TheFile.txt", 0} ) ) != "some JSON (memory) data\n" ) {
    std::cerr << "failed to get data from Git CondDB\n";
    return EXIT_FAILURE;
  }

  std::cout << "success\n";
  return EXIT_SUCCESS;
}
