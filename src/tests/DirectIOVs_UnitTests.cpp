/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the Apache version 2        *
* licence, copied verbatim in the file "COPYING".                             *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "GitCondDB.h"

#include "DBImpl.h"
#include "iov_helpers.h"

#include "test_common.h"

#include "catch2/catch.hpp"

using namespace GitCondDB::v1;

TEST_CASE( "basic use" ) {
  auto logger = std::make_shared<CapturingLogger>();

  auto db = connect(
      R"(json:{
      "Cond": {
        ".condition": "",
        "0": "v0",
        "100": "v3",
        "20": "v2"
      }
    })",
      logger );

  REQUIRE( logger->size() == 1 );
  REQUIRE( logger->contains( "using JSON data from memory" ) );

  {
    auto cond = db.get( {"HEAD", "Cond", 0} );
    CHECK( std::get<0>( cond ) == "v0" );
    CHECK( std::get<1>( cond ).since == 0 );
    CHECK( std::get<1>( cond ).until == 20 );
  }
  {
    auto cond = db.get( {"HEAD", "Cond", 10} );
    CHECK( std::get<0>( cond ) == "v0" );
    CHECK( std::get<1>( cond ).since == 0 );
    CHECK( std::get<1>( cond ).until == 20 );
  }
  {
    auto cond = db.get( {"HEAD", "Cond", 20} );
    CHECK( std::get<0>( cond ) == "v2" );
    CHECK( std::get<1>( cond ).since == 20 );
    CHECK( std::get<1>( cond ).until == 100 );
  }
  {
    auto cond = db.get( {"HEAD", "Cond", 120} );
    CHECK( std::get<0>( cond ) == "v3" );
    CHECK( std::get<1>( cond ).since == 100 );
    CHECK( std::get<1>( cond ).until == CondDB::IOV::max() );
  }
}

TEST_CASE( "IOV recursion" ) {
  auto logger = std::make_shared<CapturingLogger>();

  auto db = connect(
      R"(json:{
      "Cond": {
        ".condition": "",
        "0": "v0",
        "20": {
          ".condition": "",
          "20": "nested 1",
          "50": "nested 2"
        },
        "100": {
          ".condition": "",
          "70": "nested 3",
          "300": "nested 4"
        },
        "200": "last"
      }
    })",
      logger );

  REQUIRE( logger->size() == 1 );
  REQUIRE( logger->contains( "using JSON data from memory" ) );

  {
    auto cond = db.get( {"HEAD", "Cond", 20} );
    CHECK( std::get<0>( cond ) == "nested 1" );
    CHECK( std::get<1>( cond ).since == 20 );
    CHECK( std::get<1>( cond ).until == 50 );
  }
  {
    auto cond = db.get( {"HEAD", "Cond", 55} );
    CHECK( std::get<0>( cond ) == "nested 2" );
    CHECK( std::get<1>( cond ).since == 50 );
    CHECK( std::get<1>( cond ).until == 100 );
  }
  {
    auto cond = db.get( {"HEAD", "Cond", 150} );
    CHECK( std::get<0>( cond ) == "nested 3" );
    CHECK( std::get<1>( cond ).since == 100 );
    CHECK( std::get<1>( cond ).until == 200 );
  }
  {
    auto cond = db.get( {"HEAD", "Cond", 250} );
    CHECK( std::get<0>( cond ) == "last" );
    CHECK( std::get<1>( cond ).since == 200 );
    CHECK( std::get<1>( cond ).until == CondDB::IOV::max() );
  }
}

TEST_CASE( "point before first IOV" ) {
  auto logger = std::make_shared<CapturingLogger>();

  auto db = connect(
      R"(json:{
      "Cond": {
        ".condition": "",
        "10": "v0"
      }
    })",
      logger );

  REQUIRE( logger->size() == 1 );
  REQUIRE( logger->contains( "using JSON data from memory" ) );

  {
    auto cond = db.get( {"HEAD", "Cond", 20} );
    CHECK( std::get<0>( cond ) == "v0" );
    CHECK( std::get<1>( cond ).since == 10 );
    CHECK( std::get<1>( cond ).until == CondDB::IOV::max() );
  }
  {
    auto cond = db.get( {"HEAD", "Cond", 5} );
    CHECK( !std::get<1>( cond ).valid() );
  }
}

TEST_CASE( "empty IOVs" ) {
  auto logger = std::make_shared<CapturingLogger>();

  auto db = connect(
      R"(json:{
      "Cond": {
        ".condition": ""
      }
    })",
      logger );

  REQUIRE( logger->size() == 1 );
  REQUIRE( logger->contains( "using JSON data from memory" ) );

  {
    auto cond = db.get( {"HEAD", "Cond", 5} );
    CHECK( !std::get<1>( cond ).valid() );
  }
}

TEST_CASE( "bad keys" ) {
  auto logger = std::make_shared<CapturingLogger>();

  auto db = connect(
      R"(json:{
      "Cond": {
        ".condition": "",
        "0": "v0",
        "20xyz": "v2",
        "no_digit": "v3"
      }
    })",
      logger );

  REQUIRE( logger->size() == 1 );
  REQUIRE( logger->contains( "using JSON data from memory" ) );

  {
    auto cond = db.get( {"HEAD", "Cond", 5} );
    CHECK( std::get<0>( cond ) == "v0" );
    CHECK( std::get<1>( cond ).since == 0 );
    CHECK( std::get<1>( cond ).until == CondDB::IOV::max() );
  }
}
