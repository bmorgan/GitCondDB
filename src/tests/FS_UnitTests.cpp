/*****************************************************************************\
* (c) Copyright 2018-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the Apache version 2        *
* licence, copied verbatim in the file "COPYING".                             *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "GitCondDB.h"

#include "DBImpl.h"
#include "iov_helpers.h"

#include "test_common.h"

#include <catch2/catch.hpp>

using namespace GitCondDB::v1;

TEST_CASE( "Connection" ) {
  auto logger = std::make_shared<CapturingLogger>();

  details::FilesystemImpl db{"test_data/repo", logger};
  CHECK( logger->size() == 1 );
  CHECK( logger->contains( "using files from" ) );
  CHECK( logger->contains( "'test_data/repo'" ) );

  CHECK( db.connected() );
  db.disconnect();
  CHECK( db.connected() );
  CHECK( db.exists( "HEAD" ) );
  CHECK( db.connected() );
}

TEST_CASE( "Access" ) {
  auto logger = std::make_shared<CapturingLogger>();

  details::FilesystemImpl db{"test_data/repo", logger};
  CHECK( logger->size() == 1 );
  CHECK( logger->contains( "using files from" ) );
  CHECK( logger->contains( "'test_data/repo'" ) );

  CHECK( std::get<0>( db.get( "HEAD:TheDir/TheFile.txt" ) ) == "some uncommitted data\n" );
  CHECK( logger->size() == 3 );
  CHECK( logger->contains( 1, "accessing path test_data/repo/TheDir/TheFile.txt" ) );
  CHECK( logger->contains( 2, "found regular file" ) );

  CHECK( std::get<0>( db.get( "foobar:TheDir/TheFile.txt" ) ) == "some uncommitted data\n" );
  CHECK( logger->size() == 5 );
  CHECK( logger->contains( 3, "accessing path test_data/repo/TheDir/TheFile.txt" ) );
  CHECK( logger->contains( 4, "found regular file" ) );

  {
    auto cont = std::get<1>( db.get( "HEAD:TheDir" ) );
    CHECK( logger->size() == 7 );
    CHECK( logger->contains( 5, "accessing path test_data/repo/TheDir" ) );
    CHECK( logger->contains( 6, "found directory" ) );

    CHECK( cont.dirs == std::vector<std::string>{} );
    CHECK( cont.files == std::vector<std::string>{"TheFile.txt"} );
    CHECK( cont.root == "TheDir" );
  }
  {
    auto cont = std::get<1>( db.get( "HEAD:" ) );
    CHECK( logger->size() == 9 );
    CHECK( logger->contains( 7, "accessing path test_data/repo" ) );
    CHECK( logger->contains( 8, "found directory" ) );

    std::vector<std::string> expected{".git", "Cond", "Special", "TheDir"};
    sort( begin( cont.dirs ), end( cont.dirs ) );
    CHECK( cont.dirs == expected );
    CHECK( cont.files == std::vector<std::string>{} );
    CHECK( cont.root == "" );
  }

  {
    CHECK( db.exists( "HEAD:TheDir" ) );
    CHECK( db.exists( "HEAD:TheDir/TheFile.txt" ) );
    CHECK( db.exists( "HEAD:Special" ) );
    CHECK( db.exists( "HEAD:Special/TheLink.txt" ) );
    CHECK( db.exists( "HEAD:Special/RecurseGoodLink.txt" ) );
    CHECK_FALSE( db.exists( "HEAD:NoFile" ) );
    CHECK_FALSE( db.exists( "HEAD:Special/DanglingLink.txt" ) );
    CHECK_FALSE( db.exists( "HEAD:Special/RecurseBadLink.txt" ) );
  }

  SECTION( "access no file" ) {
    CHECK_THROWS_WITH( db.get( "HEAD:Nothing" ), "cannot resolve object HEAD:Nothing" );
    CHECK( logger->size() == 10 );
    CHECK( logger->contains( 9, "accessing path test_data/repo/Nothing" ) );
  }

  CHECK( db.commit_time( "HEAD" ) == std::chrono::time_point<std::chrono::system_clock>::max() );
}
