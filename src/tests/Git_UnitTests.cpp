/*****************************************************************************\
* (c) Copyright 2018-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the Apache version 2        *
* licence, copied verbatim in the file "COPYING".                             *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "GitCondDB.h"

#include "DBImpl.h"
#include "iov_helpers.h"

#include "test_common.h"

#include <catch2/catch.hpp>

using namespace GitCondDB::v1;

TEST_CASE( "Connection" ) {
  auto logger = std::make_shared<CapturingLogger>();

  details::GitImpl db{"test_data/repo.git", logger};
  CHECK( db.connected() );
  CHECK( logger->size() == 1 );
  CHECK( logger->contains( "opening Git repository" ) );

  db.disconnect();
  CHECK( logger->size() == 2 );
  CHECK( logger->contains( "disconnect" ) );

  CHECK_FALSE( db.connected() );
  CHECK( db.exists( "HEAD" ) );
  CHECK( logger->size() == 3 );
  CHECK( logger->contains( "opening Git repository" ) );
  CHECK( logger->contains( "repo.git" ) );

  CHECK( db.connected() );
}

void access_test( const details::GitImpl& db ) {
  auto logger = std::make_shared<CapturingLogger>();
  const_cast<details::GitImpl&>( db ).set_logger( logger );

  CHECK( std::get<0>( db.get( "HEAD:TheDir/TheFile.txt" ) ) == "some data\n" );
  CHECK( logger->size() == 2 );
  CHECK( logger->contains( 0, "get Git object HEAD:TheDir/TheFile.txt" ) );
  CHECK( logger->contains( 1, "found blob object" ) );

  {
    auto cont = std::get<1>( db.get( "HEAD:TheDir" ) );
    CHECK( logger->size() == 4 );
    CHECK( logger->contains( 2, "get Git object HEAD:TheDir" ) );
    CHECK( logger->contains( 3, "found tree object" ) );

    CHECK( cont.dirs == std::vector<std::string>{} );
    CHECK( cont.files == std::vector<std::string>{"TheFile.txt"} );
    CHECK( cont.root == "TheDir" );
  }
  {
    auto cont = std::get<1>( db.get( "HEAD:" ) );
    CHECK( logger->size() == 6 );
    CHECK( logger->contains( 4, "get Git object HEAD:" ) );
    CHECK( logger->contains( 5, "found tree object" ) );

    std::vector<std::string> expected{"Cond", "Special", "TheDir"};
    sort( begin( cont.dirs ), end( cont.dirs ) );
    CHECK( cont.dirs == expected );
    CHECK( cont.files == std::vector<std::string>{} );
    CHECK( cont.root == "" );
  }

  {
    CHECK( db.exists( "HEAD:TheDir" ) );
    CHECK( db.exists( "HEAD:TheDir/TheFile.txt" ) );
    CHECK( db.exists( "HEAD:Special" ) );
    CHECK( db.exists( "HEAD:Special/TheLink.txt" ) );
    CHECK( db.exists( "HEAD:Special/RecurseGoodLink.txt" ) );
    CHECK_FALSE( db.exists( "HEAD:NoFile" ) );
    CHECK_FALSE( db.exists( "HEAD:Special/DanglingLink.txt" ) );
    CHECK_FALSE( db.exists( "HEAD:Special/RecurseBadLink.txt" ) );
  }

  SECTION( "access no file" ) {
    CHECK_THROWS_WITH( db.get( "HEAD:Nothing" ), Catch::StartsWith( "cannot resolve object HEAD:Nothing" ) );
    CHECK( logger->size() == 7 );
    CHECK( logger->contains( 6, "get Git object HEAD:Nothing" ) );
  }

  CHECK( std::chrono::system_clock::to_time_t( db.commit_time( "HEAD" ) ) == 1483225200 );
}

TEST_CASE( "Access" ) { access_test( details::GitImpl{"test_data/repo"} ); }

TEST_CASE( "FailAccess" ) {
  CHECK_THROWS_WITH( details::GitImpl{"test_data/no-repo"}, Catch::Contains( "cannot open repository" ) );
}

TEST_CASE( "AccessBare" ) { access_test( details::GitImpl{"test_data/repo.git"} ); }
