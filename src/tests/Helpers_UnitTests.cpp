/*****************************************************************************\
* (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the Apache version 2        *
* licence, copied verbatim in the file "COPYING".                             *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "GitCondDB.h"

#include "DBImpl.h"
#include "iov_helpers.h"

#include <catch2/catch.hpp>

using namespace GitCondDB::v1;

TEST_CASE( "IOVHelpers ParseIOVs" ) {
  using GitCondDB::Helpers::get_key_iov;

  const std::string test_data{"0 a\n"
                              "100 b\n"
                              "200 c\n"
                              "300 d\n"};

  {
    auto [key, iov] = get_key_iov( test_data, 0 );
    CHECK( iov.valid() );
    CHECK( key == "a" );
    CHECK( iov.since == 0 );
    CHECK( iov.until == 100 );
  }
  {
    auto [key, iov] = get_key_iov( test_data, 100 );
    CHECK( iov.valid() );
    CHECK( key == "b" );
    CHECK( iov.since == 100 );
    CHECK( iov.until == 200 );
  }
  {
    auto [key, iov] = get_key_iov( test_data, 230 );
    CHECK( iov.valid() );
    CHECK( key == "c" );
    CHECK( iov.since == 200 );
    CHECK( iov.until == 300 );
  }
  {
    auto [key, iov] = get_key_iov( test_data, 500 );
    CHECK( iov.valid() );
    CHECK( key == "d" );
    CHECK( iov.since == 300 );
    CHECK( iov.until == GitCondDB::CondDB::IOV::max() );
  }

  {
    auto [key, iov] = get_key_iov( test_data, 240, {210, 1000} );
    CHECK( iov.valid() );
    CHECK( key == "c" );
    CHECK( iov.since == 210 );
    CHECK( iov.until == 300 );
  }
  {
    auto [key, iov] = get_key_iov( test_data, 500, {210, 1000} );
    CHECK( iov.valid() );
    CHECK( key == "d" );
    CHECK( iov.since == 300 );
    CHECK( iov.until == 1000 );
  }

  {
    auto [key, iov] = get_key_iov( test_data, 2000, {210, 1000} );
    CHECK_FALSE( iov.valid() );
    CHECK( key == "" );
  }
}

using IOV = CondDB::IOV;

TEST_CASE( "IOV Validity" ) {
  const IOV reference{10, 20};

  const IOV bad1{10, 10};
  const IOV bad2{10, 0};

  CHECK( reference.valid() );
  CHECK_FALSE( bad1.valid() );
  CHECK_FALSE( bad2.valid() );
}

TEST_CASE( "IOV Intersect" ) {
  for ( CondDB::time_point_t a : std::vector<CondDB::time_point_t>{90, 100, 110} ) {
    for ( CondDB::time_point_t b : std::vector<CondDB::time_point_t>{190, 200, 210} ) {
      const auto res   = IOV{100, 200}.intersect( IOV{a, b} );
      const auto since = std::max<CondDB::time_point_t>( a, 100 );
      const auto until = std::min<CondDB::time_point_t>( b, 200 );

      CHECK( res.since == since );
      CHECK( res.until == until );
    }
  }
  {
    const auto res = IOV{100, 200}.intersect( {0, 10} );
    CHECK_FALSE( res.valid() );
  }
  {
    const auto res = IOV{100, 200}.intersect( {10, 0} );
    CHECK_FALSE( res.valid() );
  }
}

TEST_CASE( "IOV Cut" ) {
  IOV iov{100, 200};

  CHECK( iov.since == 100 );
  CHECK( iov.until == 200 );

  iov.cut( {100, 190} );
  CHECK( iov.since == 100 );
  CHECK( iov.until == 190 );

  iov.cut( {110, 190} );
  CHECK( iov.since == 110 );
  CHECK( iov.until == 190 );

  iov.cut( {100, 180} );
  CHECK( iov.since == 110 );
  CHECK( iov.until == 180 );

  iov.cut( {120, 200} );
  CHECK( iov.since == 120 );
  CHECK( iov.until == 180 );

  iov.cut( {130, 170} );
  CHECK( iov.since == 130 );
  CHECK( iov.until == 170 );

  iov.cut( {0, 10} );
  CHECK_FALSE( iov.valid() );
}

#define CONTAINS_TRUE( x, y )                                                                                          \
  {                                                                                                                    \
    const IOV other{x, y};                                                                                             \
    CHECK( reference.contains( other ) );                                                                              \
  }

#define CONTAINS_FALSE( x, y )                                                                                         \
  {                                                                                                                    \
    const IOV other{x, y};                                                                                             \
    CHECK_FALSE( reference.contains( other ) );                                                                        \
  }

TEST_CASE( "IOV Contains" ) {
  const IOV reference{10, 20};

  CHECK( reference.contains( 10 ) );
  CHECK( reference.contains( 15 ) );
  CHECK_FALSE( reference.contains( 5 ) );
  CHECK_FALSE( reference.contains( 25 ) );

  CHECK( reference.contains( reference ) );

  CONTAINS_TRUE( 11, 19 );
  CONTAINS_TRUE( 10, 15 );
  CONTAINS_TRUE( 15, 20 );
  CONTAINS_FALSE( 5, 15 );
  CONTAINS_FALSE( 15, 25 );
  CONTAINS_FALSE( 5, 10 );
  CONTAINS_FALSE( 20, 25 );

  const IOV bad{15, 15};
  CHECK_FALSE( bad.contains( 15 ) );
  CHECK_FALSE( bad.contains( 5 ) );
  CHECK_FALSE( bad.contains( 20 ) );
  CHECK_FALSE( reference.contains( bad ) );
  CHECK_FALSE( bad.contains( reference ) );
}

#define OVERLAPS_TRUE( a, b, x, y )                                                                                    \
  {                                                                                                                    \
    const IOV reference{a, b}, other{x, y};                                                                            \
    CHECK( reference.overlaps( other ) );                                                                              \
  }

#define OVERLAPS_FALSE( a, b, x, y )                                                                                   \
  {                                                                                                                    \
    const IOV reference{a, b}, other{x, y};                                                                            \
    CHECK_FALSE( reference.overlaps( other ) );                                                                        \
  }

TEST_CASE( "IOV Overlaps" ) {
  OVERLAPS_TRUE( 10, 20, 10, 20 );
  OVERLAPS_TRUE( 10, 20, 11, 19 );
  OVERLAPS_TRUE( 10, 20, 5, 25 );

  OVERLAPS_TRUE( 10, 20, 15, 20 );
  OVERLAPS_TRUE( 10, 20, 15, 25 );
  OVERLAPS_TRUE( 10, 20, 10, 15 );
  OVERLAPS_TRUE( 10, 20, 5, 15 );

  OVERLAPS_TRUE( 10, 20, 10, 25 );
  OVERLAPS_TRUE( 10, 20, 5, 20 );

  OVERLAPS_FALSE( 10, 20, 0, 5 );
  OVERLAPS_FALSE( 10, 20, 25, 30 );
}
